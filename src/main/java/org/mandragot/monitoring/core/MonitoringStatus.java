package org.mandragot.monitoring.core;

public enum MonitoringStatus {
    NO_PROBLEMS ("AUCUN PROBLÈME"),
    NEW_PROBLEMS ("⚠ NOUVEAU(X) PROBLÈME(S)"),
    SAME_PROBLEMS ("⚠ MÊME(S) PROBLÈME(S)"),
    PROBLEMS_CHANGED ("⚠ PROBLÈME(S) DIFFÉRENT(S)"),
    PROBLEMS_SOLVED ("🎉 PROBLÈME(S) RÉSOLU(S)");

    private String label;
    MonitoringStatus(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
