package org.mandragot.monitoring.core;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.engine.TestExecutionResult.Status;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.mandragot.monitoring.notifiers.NotificationMessage;
import org.mandragot.monitoring.notifiers.Notifier;

public class Monitor {

    private final int interval;
    private final List<Notifier> notifiers = new ArrayList<Notifier>();

    private String testSuiteClass;
    private Map<TestIdentifier, TestExecutionResult> lastProblems = new HashMap<TestIdentifier, TestExecutionResult>();

    public Monitor(int interval, String testSuiteClass) {
        this.interval = interval;
        this.testSuiteClass = testSuiteClass;
    }

    private static String prettifyThrowable(Optional<Throwable> optional) {
        if (optional.isEmpty()) {
            return "<no explicit error>";
        } else {
            return optional.get().getClass().getName() + " (" + optional.get().getMessage() + ")";
        }
    }

    private static class CustomTestExecutionListener implements TestExecutionListener {

        public Map<TestIdentifier, TestExecutionResult> problems = new HashMap<TestIdentifier, TestExecutionResult>();

        @Override
        public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) {
            if (testExecutionResult.getStatus() != Status.SUCCESSFUL) {
                problems.put(testIdentifier, testExecutionResult);
            }
        }

    }

    private Map<TestIdentifier, TestExecutionResult> runTestSuite() {
        CustomTestExecutionListener customListener = new CustomTestExecutionListener();
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(DiscoverySelectors.selectClass(testSuiteClass)).build();
        Launcher launcher = LauncherFactory.create();
        launcher.registerTestExecutionListeners(customListener);
        launcher.execute(request);
        return customListener.problems;
    }

    private void sendMessage(NotificationMessage message) {
        System.out.println("Sending the following message to notifiers: " + message.summary);
        for (Notifier notifier : notifiers) {
            notifier.notify(message);
        }
    }

    private static NotificationMessage createMessage(Map<TestIdentifier, TestExecutionResult> problems,
            MonitoringStatus status) {

        String oneliner = "Nouvelle situation : " + status;
        String beginning = "[" + getTime() + "] " + oneliner + ". ";

        String title = oneliner;
        String summary = beginning;
        String full = beginning;

        if (!problems.isEmpty()) {
            String intro = "⚠️ Problème(s) détecté(s) ! Les tests suivants ont échoué : ";
            summary += intro;
            full += intro + "\n\n";

            List<String> problemsNames = new ArrayList<String>();
            List<String> problemsDescriptions = new ArrayList<String>();

            for (TestIdentifier testIdentifier : problems.keySet()) {
                TestExecutionResult testExecutionResult = problems.get(testIdentifier);
                problemsNames.add(testIdentifier.getDisplayName());
                String description = "'" + testIdentifier.getDisplayName() + "' a échoué avec l'erreur «"
                        + prettifyThrowable(testExecutionResult.getThrowable()) + "»";
                problemsDescriptions.add(description);
            }

            summary += String.join(", ", problemsNames);
            full += String.join("\n\n", problemsDescriptions);
        }

        return new NotificationMessage(title, summary, full);
    }

    private boolean compareTestResults(TestExecutionResult result1, TestExecutionResult result2) {
        return result1.toString().equals(result2.toString());
    }

    private boolean compareProblems(Map<TestIdentifier, TestExecutionResult> problems1,
            Map<TestIdentifier, TestExecutionResult> problems2) {
        if (problems1.size() != problems2.size()) {
            return false;
        }

        for (TestIdentifier id : problems1.keySet()) {
            if (!problems2.keySet().contains(id)) {
                return false;
            }
            TestExecutionResult result1 = problems1.get(id);
            TestExecutionResult result2 = problems2.get(id);
            if (!compareTestResults(result1, result2)) {
                return false;
            }

        }
        return true;
    }

    private MonitoringStatus computeStatus(Map<TestIdentifier, TestExecutionResult> newProblems) {

        // If problems have been found
        if (!newProblems.isEmpty()) {
            // If there were no problems before
            if (lastProblems.isEmpty()) {
                return MonitoringStatus.NEW_PROBLEMS;
            }

            // If the problems are the same as before
            else if (compareProblems(lastProblems, newProblems)) {
                return MonitoringStatus.SAME_PROBLEMS;
            }

            // If the problems are not the same as before
            else {
                return MonitoringStatus.PROBLEMS_CHANGED;
            }
        }

        // If no problems have been found
        else {
            if (lastProblems.isEmpty()) {
                return MonitoringStatus.NO_PROBLEMS;
            } else {
                return MonitoringStatus.PROBLEMS_SOLVED;
            }
        }

    }

    private static String getTime() {
        Instant nowUtc = Instant.now();
        ZoneId france = ZoneId.of("Europe/Paris");
        ZonedDateTime nowInFrance = ZonedDateTime.ofInstant(nowUtc, france);
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd/MM/yyyy − HH:mm:ss z");
        return nowInFrance.format(formatter2);
    }

    public void addNotifier(Notifier notifier) {
        System.out.println("Adding notifier " + notifier.getClass().getSimpleName());
        this.notifiers.add(notifier);
    }

    public void start() throws InterruptedException {
        System.out.println("Start of monitor.");
        System.out.println("Using test suite: " + this.testSuiteClass);

        if (notifiers.isEmpty()) {
            System.out.println("WARNING: No configured notifiers.");
        }

        while (true) {

            System.out.println("[" + getTime() + "] Start monitoring check…");

            // Run the test suite
            Map<TestIdentifier, TestExecutionResult> newProblems = this.runTestSuite();

            // Compute current status
            MonitoringStatus status = computeStatus(newProblems);
            System.out.println("The current status is: " + status);

            // Depending on the status we might send a notification
            switch (status) {
                case NEW_PROBLEMS:
                case PROBLEMS_CHANGED:
                case PROBLEMS_SOLVED:
                    System.out.println("The status changed, sending new notification.");
                    sendMessage(createMessage(newProblems, status));
                    break;
                case SAME_PROBLEMS:
                case NO_PROBLEMS:
                    System.out.println("The status did not change, nothing to do.");
                    break;
            }

            // In any case we replace the last observed problems
            lastProblems.clear();
            lastProblems.putAll(newProblems);

            System.out.println("End of monitoring check. Next check in " + this.interval + " ms.");
            Thread.sleep(this.interval);
        }

    }

}
