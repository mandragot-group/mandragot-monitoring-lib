package org.mandragot.monitoring.main;

import java.util.ArrayList;
import java.util.List;

import org.mandragot.monitoring.core.Monitor;
import org.mandragot.monitoring.notifiers.EmailNotifier;
import org.mandragot.monitoring.notifiers.FreeSMSNotifier;

public class Main {

    private static boolean isEmptyOrNull(String str) {
        return str == null || str.trim().isEmpty();
    }

    private static String getEnvOrDefault(String varName, String defaultValue) {
        String value = System.getenv(varName);
        if (isEmptyOrNull(value)) {
            return defaultValue;
        } else {
            return value;
        }
    }

    private static class Parameters {

        List<String> parameters = new ArrayList<String>();

        Parameters(String... parametersNames) {
            for (String parameterName : parametersNames) {
                parameters.add(System.getenv(parameterName));
            }
        }

        boolean areOK() {
            return parameters.stream().allMatch(p -> !isEmptyOrNull(p));
        }

        String getNext() {
            String next = parameters.get(0);
            parameters.remove(0);
            return next;
        }

    }

    public static void main(String[] args) throws InterruptedException {

        // Create Monitor
        String intervalString = getEnvOrDefault("INTERVAL", "1800000");
        int interval = Integer.parseInt(intervalString);
        String testSuiteClass = System.getenv("TEST_SUITE_CLASS");
        Monitor m = new Monitor(interval, testSuiteClass);

        // Try to add a FreeSMS notifier
        Parameters smsParameters = new Parameters("FREESMS_USER", "FREESMS_PASS");
        if (smsParameters.areOK()) {
            m.addNotifier(new FreeSMSNotifier(smsParameters.getNext(), smsParameters.getNext()));
        }

        // Try to add an Email notifier
        Parameters emailParameters = new Parameters("EMAIL_HOST", "EMAIL_PORT", "EMAIL_TLS", "EMAIL_USER", "EMAIL_PASS",
                "EMAIL_RECIPIENT", "TEST_SUITE_CLASS");
        if (emailParameters.areOK()) {
            m.addNotifier(new EmailNotifier(emailParameters.getNext(), Integer.parseInt(emailParameters.getNext()),
                    Boolean.parseBoolean(emailParameters.getNext()), emailParameters.getNext(),
                    emailParameters.getNext(), emailParameters.getNext()));
        }

        // Start the monitor
        m.start();
    }
}