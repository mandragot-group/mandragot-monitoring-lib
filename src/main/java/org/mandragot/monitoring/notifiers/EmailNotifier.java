package org.mandragot.monitoring.notifiers;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailNotifier implements Notifier {

    String host;
    int port;
    String userName;
    String password;
    boolean enableTLS;
    String towards;

    public EmailNotifier(String host, int port, boolean enableTLS, String userName, String password, String towards) {
        this.host = host;
        this.port = port;
        this.userName = userName;
        this.password = password;
        this.enableTLS = enableTLS;
        this.towards = towards;
    }

    private void sendEmail(String messageBody) {

        final String username = this.userName;
        final String password = this.password;

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", this.enableTLS);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", this.host);
        props.put("mail.smtp.port", this.port);

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("noreply@mandragot.org"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(this.towards));
            message.setSubject("Monitoring Mandragot");
            message.setText(messageBody);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void notify(NotificationMessage message) {
        sendEmail(message.full);

    }
}