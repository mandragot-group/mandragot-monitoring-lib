package org.mandragot.monitoring.notifiers;

public interface Notifier {
    
    public void notify(NotificationMessage message);
}
