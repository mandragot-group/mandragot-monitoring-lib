package org.mandragot.monitoring.notifiers;

public class NotificationMessage {
    
    public String title;
    public String summary;
    public String full;

    public NotificationMessage(String title,String summary, String full) {
        this.title = title;
        this.summary = summary;
        this.full = full;
    }

}
