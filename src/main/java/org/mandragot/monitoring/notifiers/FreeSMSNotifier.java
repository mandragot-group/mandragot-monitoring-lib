package org.mandragot.monitoring.notifiers;

import java.net.URL;
import java.net.URLConnection;

public class FreeSMSNotifier implements Notifier {

    String smsUser;
    String smsPass;

    public FreeSMSNotifier(String smsUser, String smsPass) {
        this.smsUser = smsUser;
        this.smsPass = smsPass;
    }

    @Override
    public void notify(NotificationMessage message) {

        String urlString = "https://smsapi.free-mobile.fr/sendmsg?user=" + smsUser + "&pass=" + smsPass + "&msg="
                + message.summary;

        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.getInputStream();
        } catch (Exception e) {
            System.out.println("Could not send SMS because of error:");
            e.printStackTrace();
        } 

    }

}
